package org.tritonus.sampled.file.jorbis;

import com.jcraft.jogg.*;
import org.tritonus.share.TDebug;
import org.tritonus.share.sampled.file.TAudioFileFormat;
import org.tritonus.share.sampled.file.TAudioFileReader;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.io.InputStream;

public class JorbisAudioFileReader extends TAudioFileReader {
    private static final int INITAL_READ_LENGTH = 4096;
    private static final int MARK_LIMIT = 4097;

    public JorbisAudioFileReader() {
        super(4097, true);
    }

    protected AudioFileFormat getAudioFileFormat(InputStream inputStream, long lFileSizeInBytes)
            throws UnsupportedAudioFileException, IOException {
        SyncState oggSyncState = new SyncState();
        StreamState oggStreamState = new StreamState();
        Page oggPage = new Page();
        Packet oggPacket = new Packet();
        int bytes = 0;
        oggSyncState.init();
        int index = oggSyncState.buffer(4096);
        bytes = inputStream.read(oggSyncState.data, index, 4096);
        oggSyncState.wrote(bytes);
        if (oggSyncState.pageout(oggPage) != 1) {
            if (bytes < 4096)
                throw new UnsupportedAudioFileException("not a Vorbis stream: ended prematurely");
            throw new UnsupportedAudioFileException("not a Vorbis stream: not in Ogg bitstream format");
        }
        oggStreamState.init(oggPage.serialno());
        if (oggStreamState.pagein(oggPage) < 0)
            throw new UnsupportedAudioFileException("not a Vorbis stream: can't read first page of Ogg bitstream data");
        if (oggStreamState.packetout(oggPacket) != 1)
            throw new UnsupportedAudioFileException("not a Vorbis stream: can't read initial header packet");

        Buffer oggPacketBuffer = new Buffer();
        oggPacketBuffer.readinit(oggPacket.packet_base, oggPacket.packet, oggPacket.bytes);
        int nPacketType = oggPacketBuffer.read(8);
        byte[] buf = new byte[6];
        oggPacketBuffer.read(buf, 6);
        if ((buf[0] != 118) || (buf[1] != 111) || (buf[2] != 114) || (buf[3] != 98) ||
                (buf[4] != 105) || (buf[5] != 115))
            throw new UnsupportedAudioFileException("not a Vorbis stream: not a vorbis header packet");

        if (nPacketType != 1)
            throw new UnsupportedAudioFileException("not a Vorbis stream: first packet is not the identification header");
        if (oggPacket.b_o_s == 0)
            throw new UnsupportedAudioFileException("not a Vorbis stream: initial packet not marked as beginning of stream");
        int nVersion = oggPacketBuffer.read(32);
        if (nVersion != 0)
            throw new UnsupportedAudioFileException("not a Vorbis stream: wrong vorbis version");
        int nChannels = oggPacketBuffer.read(8);
        float fSampleRate = oggPacketBuffer.read(32);
        int bitrate_upper = oggPacketBuffer.read(32);
        int bitrate_nominal = oggPacketBuffer.read(32);
        int bitrate_lower = oggPacketBuffer.read(32);
        int[] blocksizes = new int[2];
        blocksizes[0] = (1 << oggPacketBuffer.read(4));
        blocksizes[1] = (1 << oggPacketBuffer.read(4));

        if ((fSampleRate < 1.0F) || (nChannels < 1) || (blocksizes[0] < 8)
                || (blocksizes[1] < blocksizes[0])
                || (oggPacketBuffer.read(1) != 1))
            throw new UnsupportedAudioFileException("not a Vorbis stream: illegal values in initial header");
        if (TDebug.TraceAudioFileReader)
            TDebug.out("JorbisAudioFileReader.getAudioFileFormat(): channels: " + nChannels);
        if (TDebug.TraceAudioFileReader)
            TDebug.out("JorbisAudioFileReader.getAudioFileFormat(): rate: " + fSampleRate);

        int nByteSize = -1;
        if ((lFileSizeInBytes != -1L) && (lFileSizeInBytes <= 2147483647L))
            nByteSize = (int) lFileSizeInBytes;
        int nFrameSize = -1;
        AudioFormat format = new AudioFormat(new AudioFormat.Encoding("VORBIS"), fSampleRate, -1, nChannels, -1, -1.0F, true);
        if (TDebug.TraceAudioFileReader)
            TDebug.out("JorbisAudioFileReader.getAudioFileFormat(): AudioFormat: " + format);
        AudioFileFormat.Type type = new AudioFileFormat.Type("Ogg", "ogg");
        AudioFileFormat audioFileFormat = new TAudioFileFormat(type, format, nFrameSize, nByteSize);

        if (TDebug.TraceAudioFileReader)
            TDebug.out("JorbisAudioFileReader.getAudioFileFormat(): AudioFileFormat: " + audioFileFormat);
        return audioFileFormat;

    }

}